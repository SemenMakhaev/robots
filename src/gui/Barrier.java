package gui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public final class Barrier {
	
	private ArrayList<Point> vertices;
	private final Color color; 
	// 0---1
	// |   |
	// 3---2
	
	public Barrier(ArrayList<Point> points)
	{
		vertices = new ArrayList<Point>();
		for (Point point : points)
			vertices.add(point);
		
		ArrayList<Color> colors = new ArrayList<Color>();
		colors.add(Color.BLUE);
		colors.add(Color.GREEN);
		colors.add(Color.ORANGE);
		colors.add(Color.RED);
		colors.add(Color.BLUE);
		colors.add(Color.YELLOW);
		
		Random randomGenerator = new Random();
		int randomInteger = randomGenerator.nextInt(colors.size() - 1);
		color = colors.get(randomInteger);
	}
	
	public Color getColor()
	{
		return color;
	}
	
	public ArrayList<Point> getVertices()
	{
		return vertices;
	}
	
	public boolean lineIsNotIntersect(Point start, Point finish)
	{
		if (isPointInsideRectangle(start) || isPointInsideRectangle(finish))
			return false;
		if (isLineIntersectLine(start, finish, vertices.get(0), vertices.get(1)))
			return false;
		if (isLineIntersectLine(start, finish, vertices.get(1), vertices.get(2)))
			return false;
		if (isLineIntersectLine(start, finish, vertices.get(2), vertices.get(3)))
			return false;
		if (isLineIntersectLine(start, finish, vertices.get(3), vertices.get(0)))
			return false;
		return true;
	}
	
	private boolean isPointInsideRectangle(Point point)
	{
		return point.getX() >= vertices.get(0).getX()
				&& point.getX() < vertices.get(1).getX()
				&& point.getY() >= vertices.get(0).getY()
				&& point.getY() < vertices.get(3).getY();
	}

	private boolean isLineIntersectLine(Point thisStart, Point thisEnd, Point thatStart, Point thatEnd)
	{
		int s1 = sameSide (thisStart, thisEnd, thatStart, thatEnd);
	    int s2 = sameSide (thatStart, thatEnd, thisStart, thisEnd);
	    
	    return s1 <= 0 && s2 <= 0;
	}
	
	private static int sameSide (Point lineStart, Point lineEnd, Point firstPoint, Point secondPoint)
	{
		int  sameSide = 0;
		
		double dx  = lineEnd.getX() - lineStart.getX();
		double dy  = lineEnd.getY() - lineStart.getY();
		double dx1 = firstPoint.getX() - lineStart.getX();
		double dy1 = firstPoint.getY() - lineStart.getY();
		double dx2 = secondPoint.getX() - lineEnd.getX();
		double dy2 = secondPoint.getY() - lineEnd.getY();
		
		double c1 = dx * dy1 - dy * dx1;
		double c2 = dx * dy2 - dy * dx2;
		
		if (c1 != 0 && c2 != 0)
			sameSide = c1 < 0 != c2 < 0 ? -1 : 1;
		else if (dx == 0 && dx1 == 0 && dx2 == 0)
			sameSide = !coordinateIsInRange (lineStart.getY(), lineEnd.getY(), firstPoint.getY())
			&& !coordinateIsInRange (lineStart.getY(), lineEnd.getY(), secondPoint.getY()) ? 1 : 0;
		else if (dy == 0 && dy1 == 0 && dy2 == 0)
			sameSide = !coordinateIsInRange (lineStart.getX(), lineEnd.getX(), firstPoint.getX())
			&& !coordinateIsInRange (lineStart.getX(), lineEnd.getX(), secondPoint.getX()) ? 1 : 0;
		
		return sameSide;
	}
	
	private static boolean coordinateIsInRange (double bottom, double coordinate, double top)
	{
		return coordinate > bottom ?
				top >= bottom && top <= coordinate :
					top >= coordinate && top <= bottom;
	}
}
