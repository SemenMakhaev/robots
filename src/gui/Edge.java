package gui;

public final class Edge {

	private final Point thisVertex;
	private final Point thatVertex;
	
	public Edge(Point thisVertex, Point thatVertex) {
		this.thisVertex = thisVertex;
		this.thatVertex = thatVertex;
	}
	
	public Point getThisVertex() {
		return thisVertex;
	}
	
	public Point getThatVertex() {
		return thatVertex;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (!(obj instanceof Edge))
		{
			return false;
		} 
		
		Edge edge = (Edge) obj;
		
		return edge.thisVertex == thisVertex
				&& edge.thatVertex == thatVertex ||
				edge.thisVertex == thatVertex &&
				edge.thatVertex == thisVertex;
	}
}
