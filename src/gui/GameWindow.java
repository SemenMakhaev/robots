package gui;

import java.awt.BorderLayout;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;

public class GameWindow extends JInternalFrame implements Serializable, Observer
{
	private static final long serialVersionUID = 1L;
	private final GameVisualizer m_visualizer;
	private final Robot robot;

    public GameWindow(Robot robot, Barrier [] barriers) 
    {
        super("Р�РіСЂРѕРІРѕРµ РїРѕР»Рµ", true, true, true, true);
        m_visualizer = new GameVisualizer(robot, barriers);
        this.robot = robot;
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(m_visualizer, BorderLayout.CENTER);
        getContentPane().add(panel);
        pack();
    }
    
    public Robot getRobot()
    {
    	return robot;
    }

	@Override
	public void update(Observable o, Object arg)
	{
		try
		{
			sendToServer(InetAddress.getLocalHost(), RobotsServer.PORT);
		}
		catch (UnknownHostException e)
		{
			e.printStackTrace();
		}
		m_visualizer.onModelUpdateEvent();
	}
	
	private void sendToServer(InetAddress host, int port)
	{
		try
		{
			Socket server = new Socket(host, port);
			try
			{
				OutputStream os = server.getOutputStream();
				PrintWriter pw = new PrintWriter(new OutputStreamWriter(os, "Cp1251"));
				pw.print(String.format("%s, %s, %s, %s",
						robot.getX(), robot.getY(), robot.getTargetX(), robot.getTargetY()));
				pw.flush();
			}
			finally
			{
				server.close();
			}
		}
		catch (ConnectException e)
		{
			// Just ignore.
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
