package gui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Robot extends Observable implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static final double comparisonEpsilon = 1; 
	private volatile double positionX;
	private volatile double positionY;
	private volatile double direction = 0;
	private volatile double duration = 10;
	
	private volatile int targetX = 150;
	private volatile int targetY = 100;
	
	private volatile ArrayList<Point> path = new ArrayList<Point>();
	private volatile boolean switched;
	
	public static final double maxVelocity = 0.1; 
    public static final double maxAngularVelocity = 0.001;
    private ArrayList<Observer> observers = new ArrayList<Observer>();
	
	public Robot()
	{
		positionX = 100;
		positionY = 100;
	}
	
	public Robot(int x, int y)
	{
		positionX = x;
		positionY = y;
	}
	
	public double getX()
	{
		return positionX;
	}
	
	public double getY()
	{
		return positionY;
	}
	
	public double getDirection()
	{
		return direction;
	}
	
	public int getTargetX()
	{
		return targetX;
	}
	
	public int getTargetY()
	{
		return targetY;
	}
	
	public double getDuration()
	{
		return duration;
	}
	
	public boolean targetReached()
	{
		return Math.abs(positionX - targetX) < comparisonEpsilon
				&& Math.abs(positionY - targetY) < comparisonEpsilon;
	}
	
	public void setPosition(double x, double y)
	{
		positionX = x;
		positionY = y;
	}
	
	public void setTargetPosition(Point p)
	{
		targetX = p.getX();
		targetY = p.getY();
	}
	
	public void setDirection(double direction)
	{
		this.direction = direction;
	}
	
	public void setPath(ArrayList<Point> path)
	{
		this.path = path;
	}
    
    private static double angleTo(double fromX, double fromY, double toX, double toY)
    {
        double diffX = toX - fromX;
        double diffY = toY - fromY;
        
        return Robot.asNormalizedRadians(Math.atan2(diffY, diffX));
    }
    
    private static double distance(double x1, double y1, double x2, double y2)
    {
        double diffX = x1 - x2;
        double diffY = y1 - y2;
        return Math.sqrt(diffX * diffX + diffY * diffY);
    }
	
	@Override
	public void addObserver(Observer observer)
	{
		if (observer != null)
			observers.add(observer);
	}
	
	@Override
	public void deleteObserver(Observer observer)
	{
		if (observer != null)
			observers.remove(observer);
	}
	
	@Override
	public void notifyObservers()
	{
		for (Observer observer : observers)
			observer.update(this, null);
	}
	
	public void start()
	{
		switched = true;
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				while (switched)
				{
					synchronized (this)
					{
						if (targetReached())
						{
							if (path != null && path.size() > 0)
					    	{
					    		Point followingTarget = path.get(path.size() - 1);
					    		path.remove(path.size() - 1);
					    		setTargetPosition(followingTarget);
					    	}
						}
						else
						{
					        double distance = distance(targetX, targetY, positionX, positionY);
					        if (distance < 0.5) 
				        	{
					        	return;
				        	}
					        
					        double velocity = maxVelocity;
					        double angleToTarget = angleTo(positionX, positionY, targetX, targetY);
					        double angularVelocity = 0;
					        if (angleToTarget > direction)
					        {
					            angularVelocity = maxAngularVelocity;
					        }
					        if (angleToTarget < direction)
					        {
					            angularVelocity = -maxAngularVelocity;
					        }
					        
					        move(velocity, angularVelocity, angleToTarget);
					        try
					        {
								Thread.sleep((long) duration);
							}
					        catch (InterruptedException e)
					        {
					        	stop();
							}
						}
					}
				}
			}
		}).start();
	}
	
	public void stop()
	{
		switched = false;
	}
    
    private static double applyLimits(double value, double min, double max)
    {
        if (value < min)
            return min;
        if (value > max)
            return max;
        return value;
    }
    
    public void move(double velocity, double angularVelocity, double angleToTarget)
    {
        velocity = applyLimits(velocity, 0, maxVelocity);
        angularVelocity = applyLimits(angularVelocity, -maxAngularVelocity, maxAngularVelocity);
        double newX = positionX + velocity / angularVelocity * 
            (Math.sin(direction  + angularVelocity * duration) -
                Math.sin(direction));
        if (!Double.isFinite(newX))
        {
            newX = positionX + velocity * duration * Math.cos(direction);
        }
        double newY = positionY - velocity / angularVelocity * 
            (Math.cos(direction  + angularVelocity * duration) -
                Math.cos(direction));
        if (!Double.isFinite(newY))
        {
            newY = positionY + velocity * duration * Math.sin(direction);
        }
        if (Math.abs(direction - angleToTarget) < 0.01)
        {
	        positionX = newX;
	        positionY = newY;
        }
        double newDirection;
        if (Math.abs(direction - angleToTarget) > Math.PI)
        {
        	newDirection = asNormalizedRadians(direction - angularVelocity * duration); 
        } else {
        	newDirection = asNormalizedRadians(direction + angularVelocity * duration);
        }
        direction = newDirection;
        notifyObservers();
    }
    
    public static double asNormalizedRadians(double angle)
    {
        while (angle < 0)
        {
            angle += 2*Math.PI;
        }
        while (angle >= 2*Math.PI)
        {
            angle -= 2*Math.PI;
        }
        return angle;
    }
    
    @Override
    public String toString()
    {
    	return String.format("Robot(%s, %s)", positionX, positionY);
    }

}
