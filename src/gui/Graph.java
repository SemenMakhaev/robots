package gui;

import java.util.ArrayList;
import java.util.HashMap;

public class Graph {

	private ArrayList<Point> vertices;
	private ArrayList<Edge> edges;

	public Graph() {
		vertices = new ArrayList<Point>();
		edges = new ArrayList<Edge>();
	}
	
	public void addVertex(Point vertex) {
		vertices.add(vertex);
	}
	
	public ArrayList<Edge> getEdges() {
		return edges;
	}
	
	// Creation edges that not intersect given barriers on vertices of the current graph. 
	public void createEdges(Barrier [] barriers)
	{
		for (Point thisVertex : vertices)
		{
			for (Point thatVertex : vertices)
			{
				boolean intersects = false;
				
				for (Barrier barrier : barriers)
				{
					if (!barrier.lineIsNotIntersect(thisVertex, thatVertex))
					{
						intersects = true;
						break;
					}
				}
				
				if (!intersects)
				{
					Edge edge = new Edge(thisVertex, thatVertex);
					edges.add(edge);
					thisVertex.addIncidentEdge(edge);
					thatVertex.addIncidentEdge(edge);
				}
			}
		}
	}
	
	// Breath first search.
	public ArrayList<Point> getPath(Point start, Point finish)
	{
		if (finish.getIncidentEdges().size() == 0 || start.getIncidentEdges().size() == 0)
		{
			return null;
		}
		
		ArrayList<Point> queue = new ArrayList<Point>();
		HashMap<Point, Point> parent = new HashMap<Point, Point>();
		queue.add(start);
		start.visit();
		Point current = null;
		
		while (current != finish)
		{
			current = queue.get(0);
			queue.remove(0);
			
			for (Edge edge : current.getIncidentEdges())
			{
				Point oppositVertex = edge.getThisVertex(); 
				
				if (current == oppositVertex)
				{
					oppositVertex = edge.getThatVertex();
				}
				
				if (!oppositVertex.isVisited())
				{
					queue.add(oppositVertex);
					oppositVertex.visit();
					parent.put(oppositVertex, current);
				}
			}
		}
		
		ArrayList<Point> path = new ArrayList<Point>();
		current = finish;
		while (current != start)
		{
			path.add(current);
			current = parent.get(current);
		}

		return path;
	}
}
