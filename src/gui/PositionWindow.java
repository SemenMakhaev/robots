package gui;

import java.awt.BorderLayout;
import java.awt.TextArea;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;

public class PositionWindow extends JInternalFrame implements Observer {

	private static final long serialVersionUID = 1L;
	private TextArea text;
	
	public PositionWindow() {
		super("Координаты робота", true, true, true);
		text = new TextArea();
		
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(text, BorderLayout.CENTER);
		getContentPane().add(panel);
		pack();
		
		text.setText("Буферизация...");
	}

	@Override
	public void update(Observable observable, Object arg) {
		Robot robot = (Robot) observable;
		text.setText(String.format("X: %s\nY: %s",
				robot.getX(), robot.getY()));
	}

}
