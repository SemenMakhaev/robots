package gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import log.Logger;

public class MainApplicationFrame extends JFrame implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private File serializationDirectory;
	private File barriersSerializationDirectory;
	private File robotsState;
	private RobotsLoader robotsLoader = new RobotsLoader();
	private JDesktopPane desktopPane = new JDesktopPane();
	private ArrayList<Robot> robots;
	private Robot currentRobot;
	private Barrier [] barriers;
	
    public MainApplicationFrame()
    {
    	serializationDirectory = new File(System.getProperty("user.home")
    			+ File.separator + "restoreConfigFile");
    	barriersSerializationDirectory = new File(System.getProperty("user.home")
    			+ File.separator + "barriersConfig.txt");
    	robotsState = new File(System.getProperty("user.home")
    			+ File.separator + "robotsState");
    	robots = new ArrayList<Robot>();
    	
    	if (!robotsState.exists() || !deserializeRobots())
    	{
        	currentRobot = createRobot();
        	robots.add(currentRobot);
        }
    	
    	if (barriersSerializationDirectory.exists())
    	{
    		deserializeBarriers();
    	}
    	
        int inset = 50;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(inset, inset,
            screenSize.width  - inset*2,
            screenSize.height - inset*2);

        setContentPane(desktopPane);
        addMouseListener(new MouseAdapter()
		{
        	@Override
        	public void mouseClicked(MouseEvent e)
        	{
        		JInternalFrame currentFrame = desktopPane.getSelectedFrame();
        		if (currentFrame instanceof GameWindow)
        		{
        			currentRobot = ((GameWindow)currentFrame).getRobot();
        		}
        	}
		});

        setJMenuBar(generateMenuBar());
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        if (!serializationDirectory.exists() || !deserializeFrame())
        {
        	LogWindow logWindow = createLogWindow();
            addWindow(logWindow);

            if (currentRobot != null)
            {
	            GameWindow gameWindow = new GameWindow(currentRobot, barriers);
	            currentRobot.addObserver(gameWindow);
	            gameWindow.setSize(400,  400);
	            addWindow(gameWindow);
            }
        }
    }
    
    @SuppressWarnings("unchecked")
	private Robot createRobot()
    {
    	String className = "Robot";
    	Class<Robot> clazz = null;
    	Robot robot = null;
    	try
	    {
			clazz = (Class<Robot>)robotsLoader.loadClass(className);
			robot = clazz.newInstance();
    	}
    	catch (Throwable e)
    	{
    		robot = new Robot();
    	}
		return robot;
    }
    
    protected LogWindow createLogWindow()
    {
        LogWindow logWindow = new LogWindow(Logger.getDefaultLogSource());
        logWindow.setLocation(10,10);
        logWindow.setSize(300, 800);
        setMinimumSize(logWindow.getSize());
        logWindow.pack();
        Logger.debug("Протокол работает");
        return logWindow;
    }
    
    protected void addWindow(JInternalFrame frame)
    {
        desktopPane.add(frame);
        frame.setVisible(true);
    }
    
    private JMenuBar generateMenuBar()
    {
    	JMenuBar menuBar = new JMenuBar();

    	menuBar.add(getNewWindowMenu());
        menuBar.add(getViewModeMenu());
        menuBar.add(getTestMenu());
        menuBar.add(getExitMenu());
        
        return menuBar;
    }
    
    private JMenu getNewWindowMenu()
    {
    	JMenu newWindow = new JMenu("Новая игра");
    	
    	newWindow.setMnemonic(KeyEvent.VK_N);
    	newWindow.getAccessibleContext().setAccessibleDescription("Новая игра");
    	
    	{
    		JMenuItem visualizationWindow = new JMenuItem("Визуализация робота", KeyEvent.VK_V);
    		visualizationWindow.addActionListener((event) -> {
    			Robot robot = createRobot();
    			robots.add(robot);
    			robot.start();
    			GameWindow gameWindow = new GameWindow(robot, barriers);
    			robot.addObserver(gameWindow);
    	        gameWindow.setSize(400,  400);
    	        addWindow(gameWindow);
    		});
    		newWindow.add(visualizationWindow);
    	}
    	
    	{
    		JMenuItem protocolWindow = new JMenuItem("Протокол", KeyEvent.VK_P);
    		protocolWindow.addActionListener((event) -> {
    			LogWindow logWindow = createLogWindow();
    	        addWindow(logWindow);
    		});
    		newWindow.add(protocolWindow);
    	}
    	
    	{
    		JMenuItem positionWindow = new JMenuItem("Координаты робота", KeyEvent.VK_K);
    		positionWindow.addActionListener((event) -> {
    			PositionWindow position = new PositionWindow();
    			position.setSize(200, 200);
    			addWindow(position);
    			currentRobot.addObserver(position);
    		});
    		newWindow.add(positionWindow);
    	}
    	
    	return newWindow;
    }
    
    private JMenu getViewModeMenu()
    {
        JMenu lookAndFeelMenu = new JMenu("Режим отображения");
        lookAndFeelMenu.setMnemonic(KeyEvent.VK_V);
        lookAndFeelMenu.getAccessibleContext().setAccessibleDescription("Управление режимом отображения робота");
        
        {
            JMenuItem systemLookAndFeel = new JMenuItem("Системная схема", KeyEvent.VK_S);
            systemLookAndFeel.addActionListener((event) -> {
                setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                this.invalidate();
            });
            lookAndFeelMenu.add(systemLookAndFeel);
        }

        {
            JMenuItem crossplatformLookAndFeel = new JMenuItem("Универсальная схема", KeyEvent.VK_S);
            crossplatformLookAndFeel.addActionListener((event) -> {
                setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
                this.invalidate();
            });
            lookAndFeelMenu.add(crossplatformLookAndFeel);
        }
        
        return lookAndFeelMenu;
    }
    
    private JMenu getTestMenu()
    {
    	JMenu testMenu = new JMenu("Тесты");
    	
        testMenu.setMnemonic(KeyEvent.VK_T);
        testMenu.getAccessibleContext().setAccessibleDescription(
                "Тестовые команды");
        
        {
            JMenuItem addLogMessageItem = new JMenuItem("Сообщение в лог", KeyEvent.VK_S);
            addLogMessageItem.addActionListener((event) -> {
                Logger.debug("Новая строка");
            });
            testMenu.add(addLogMessageItem);
        }
        
        return testMenu;
    }
    
    private JMenu getExitMenu()
    {
    	JMenu exitMenu = new JMenu("Выход");
    	
    	exitMenu.setMnemonic(KeyEvent.VK_E);
    	exitMenu.getAccessibleContext().setAccessibleDescription("Выход");
    	{
    		JMenuItem applicationExit = new JMenuItem("Сохранить и выйти", KeyEvent.VK_ESCAPE);
    		applicationExit.addActionListener((event) -> {
    			if (JOptionPane.showOptionDialog(getParent(), "Вы уверены?", "Подтвердите",
    					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
    					new String [] {"Да", "Нет"}, null) == JOptionPane.YES_OPTION)
    			{
    				for (Robot robot : robots)
    				{
    					robot.stop();
    				}
	    			if (!serializationDirectory.exists())
	    			{
						try
						{
							serializationDirectory.createNewFile();
							if (barriers != null)
							{
								barriersSerializationDirectory.createNewFile();
							}
							robotsState.createNewFile();
						}
						catch (Exception e)
						{
							System.out.println("Can't get the access to the home directory");
						}
	    			}
	    			serializeFrame();
	    			if (barriers != null)
	    			{
	    				serializeBarriers();
	    			}
	    			serializeRobotsState();
	    			Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(
	    			         new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    			}
    		});
    		exitMenu.add(applicationExit);
    	}
    	
    	return exitMenu;
    }
    
    private void serializeFrame()
    {
    	try
    	{
    		FileWriter writer = new FileWriter(serializationDirectory);
    		try
    		{
    			for (Component window : desktopPane.getComponents())
    				if (window instanceof GameWindow
    						|| window instanceof LogWindow
    						|| window instanceof PositionWindow)
    				{
    					String windowState = String.format("%s: %s, %s%s", window.getClass().getName(),
    							window.getX(), window.getY(), System.getProperty("line.separator"));
    					writer.write(windowState);
    				}
    		}
    		finally
    		{
				writer.close();
			}
    	}
    	catch (IOException ex) {}
    }
    
    private void serializeBarriers()
    {
    	try
    	{
    		FileWriter writer = new FileWriter(barriersSerializationDirectory);
    		try
    		{
    			for (Barrier barrier : barriers)
    			{
    				StringBuilder verticies = new StringBuilder("");
    				for (Point vertex : barrier.getVertices())
    					verticies.append(String.format("(%s, %s); ", vertex.getX(), vertex.getY()));
    				writer.write(verticies.toString().substring(0, verticies.length() - 2)
    						+ System.getProperty("line.separator"));
    			}
    		}
    		finally
    		{
    			writer.close();
    		}
    	}
    	catch (IOException e) { }
    }
    
    private void serializeRobotsState()
    {
    	try
    	{
    		FileWriter writer = new FileWriter(robotsState);
    		try
    		{
    			for (Robot robot : robots)
    			{
	    			writer.write(String.format("gui.Robot: %s, %s, %s, %s, %s%s",
	    					robot.getX(), robot.getY(), robot.getDirection(),
	    					robot.getTargetX(), robot.getTargetY(), System.getProperty("line.separator")));
    			}
    		}
    		finally
    		{
    			writer.close();
    		}
    	}
    	catch (IOException e) {}
    }
    
    private boolean deserializeFrame() {
    	try {
			FileReader reader = new FileReader(serializationDirectory);
			try {
				String fullString = "";
				int ch;
				while ((ch = reader.read())!=-1) fullString += (char)ch;
				String [] strings = fullString.split(System.getProperty("line.separator"));
				int robotIdx = 0;
				for (String s : strings) {
					int x = Integer.parseInt(s.substring(s.indexOf(":")+2, s.indexOf(",")));
					int y = Integer.parseInt(s.substring(s.indexOf(",")+2));
					if (s.startsWith("gui.LogWindow")) {
						LogWindow logWindow = createLogWindow();
						logWindow.setLocation(x, y);
			            addWindow(logWindow);
					}
					if (s.startsWith("gui.GameWindow")) {
						Robot robot;
						if (robotIdx < robots.size())
						{
							robot = robots.get(robotIdx);
							robotIdx++;
						}
						else
						{
							robot = createRobot();
							robots.add(robot);
						}
						
						GameWindow gameWindow = new GameWindow(robot, barriers);
						robot.addObserver(gameWindow);
						robot.start();
						gameWindow.setLocation(x, y);
						gameWindow.setSize(400, 400);
			            addWindow(gameWindow);
					}
					if (s.startsWith("gui.PositionWindow")) {
						Robot robot;
						if (robotIdx < robots.size())
						{
							robot = robots.get(robotIdx);
							robotIdx++;
						}
						else
						{
							robot = createRobot();
						}
						
						PositionWindow positionWindow = new PositionWindow();
						positionWindow.setLocation(x, y);
						positionWindow.setSize(200, 200);
						addWindow(positionWindow);
						robot.addObserver(positionWindow);
					}
				}
			} catch (IOException e) {
				return false;
			} finally {
				try {
					reader.close();
				} catch (IOException e) {
					return false;
				}
			}
		} catch (FileNotFoundException ex) {
			return false;
		}
    	return true;
    }
    
    private void deserializeBarriers() {
    	try {
    		FileReader reader = new FileReader(barriersSerializationDirectory);
    		try {
    			String fullString = "";
				int ch;
				while ((ch = reader.read())!=-1) fullString += (char)ch;
				String [] lines = fullString.split(System.getProperty("line.separator"));
				Barrier [] serialized = new Barrier [lines.length];
				for (int barrierIdx = 0; barrierIdx < lines.length; barrierIdx++) {
					ArrayList<Point> points = new ArrayList<Point>();
					String [] coordinates = lines[barrierIdx].split("; ");
					for (String coordinate : coordinates) {
						String [] positions = coordinate.split(", ");
						int x = Integer.parseInt(positions[0].substring(1));
						int y = Integer.parseInt(positions[1].substring(0,
								positions[1].length()-1));
						points.add(new Point(x, y));
					}
					serialized[barrierIdx] = new Barrier(points);
				}
				barriers = serialized;
    		} catch (IOException e) {} finally {
    			try {
					reader.close();
				} catch (IOException e) { }
			}
		} catch (FileNotFoundException ex) { }
    }
    
    private boolean deserializeRobots() {
    	try {
    		FileReader reader = new FileReader(robotsState);
    		try {
    			String fullString = "";
				int ch;
				while ((ch = reader.read())!=-1) fullString += (char)ch;
				fullString = fullString.substring(fullString.indexOf(':')+2);
				String [] lines = fullString.split(System.getProperty("line.separator"));
				for (String line : lines)
				{
					String [] positions = line.split(", ");
					positions[0] = positions[0].substring(positions[0].indexOf(':') + 2);
					currentRobot = createRobot();
					currentRobot.setPosition(
							Integer.parseInt(positions[0].substring(0,
									positions[0].indexOf('.'))+""),
							Integer.parseInt(positions[1].substring(0,
									positions[1].indexOf('.'))+""));
					currentRobot.setDirection(Integer.parseInt(positions[2].substring(0,
							positions[2].indexOf('.'))+""));
					currentRobot.setTargetPosition(new Point(
							Integer.parseInt(positions[3]+""),
							Integer.parseInt(positions[4]+"")));
					robots.add(currentRobot);
				}
    		} catch (IOException e) {
				return false;
			} finally {
				try {
					reader.close();
				} catch (IOException e) {
					return false;
				}
			}
		} catch (FileNotFoundException ex) {
			return false;
		}
    	return true;
    }
    
    private void setLookAndFeel(String className)
    {
        try
        {
            UIManager.setLookAndFeel(className);
            SwingUtilities.updateComponentTreeUI(this);
        }
        catch (ClassNotFoundException | InstantiationException
            | IllegalAccessException | UnsupportedLookAndFeelException e)
        {
            // just ignore
        }
    }
}
