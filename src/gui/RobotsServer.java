package gui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class RobotsServer
{
	public final static int PORT = 40000;
	private static HashMap<InetAddress, Robot> robots;

	public static void main(String[] args)
	{
		robots = new HashMap<InetAddress, Robot>();
		try
		{
			ServerSocket sock = new ServerSocket(PORT);
			try
			{
				while (true)
				{
					Socket socket = sock.accept();
					InetAddress address = socket.getInetAddress();
					System.out.println(String.format("Connected: %s", address));
					if (!robots.containsKey(address))
					{
						Robot robot = new Robot();
						robots.put(address, robot);
					}
					clientServing(socket);
					System.out.println(String.format("Robots states: %s", robots));
				}
			}
			finally
			{
				sock.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void clientServing(Socket socket)
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					InputStream is = null;
					try
					{
						is = socket.getInputStream();
						BufferedReader reader = new BufferedReader(new InputStreamReader(is, "Cp1251"));
						String line = reader.readLine();// Format: "x, y, targetX, targetY"
						String [] positions = line.split(", ");
						synchronized (robots)
						{
							Robot robot = robots.get(socket.getInetAddress());
							robot.setPosition(
									Double.parseDouble(positions[0]), Double.parseDouble(positions[1]));
							robot.setTargetPosition(new Point(
									Integer.parseInt(positions[2]), Integer.parseInt(positions[3])));
						}
					}
					finally
					{
						is.close();
					}
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
				try
				{
					socket.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}).start();
	}
}
