package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JPanel;

public class GameVisualizer extends JPanel implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private static final int robotRadius = 10;
	private Barrier [] barriers;
	private Graph graph;
    private final Robot robot;
    private Point targetPoint; 
    
    public GameVisualizer(Robot robot, Barrier [] barriers) 
    {
    	this.robot = robot;
    	this.barriers = barriers;
    	targetPoint = new Point(robot.getTargetX(), robot.getTargetY());
    	graph = new Graph();

        addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
            	synchronized (graph)
            	{
	            	targetPoint = new Point(
	            			(int)e.getPoint().getX(), (int)e.getPoint().getY());
	            	Point robotPoint = new Point((int)robot.getX(), (int)robot.getY());
	            	ArrayList<Point> path = new ArrayList<Point>();
	            	if (barriers != null) {
		            	graph = new Graph();
		            	graph.addVertex(robotPoint);
		            	graph.addVertex(targetPoint);
		            	for (Barrier barrier : barriers) {
		            		ArrayList<Point> points = barrier.getVertices();
		            		graph.addVertex(new Point(
		            				points.get(0).getX()-robotRadius,
		            				points.get(0).getY()-robotRadius));
		            		graph.addVertex(new Point(
		            				points.get(1).getX()+robotRadius,
		            				points.get(1).getY()-robotRadius));
		            		graph.addVertex(new Point(
		            				points.get(2).getX()+robotRadius,
		            				points.get(2).getY()+robotRadius));
		            		graph.addVertex(new Point(
		            				points.get(3).getX()-robotRadius,
		            				points.get(3).getY()+robotRadius));
		            	}
		            	graph.createEdges(barriers);
		            	path = graph.getPath(robotPoint, targetPoint);
		            	robot.setPath(path);
	            	}
	            	else 
	        		{
	            		robot.setTargetPosition(targetPoint);
	        		}
	            	repaint();
            	}
            }
        });
        setDoubleBuffered(true);
    }
    
	public void onModelUpdateEvent()
    {
    	EventQueue.invokeLater(this::repaint);
	}

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        Graphics2D g2d = (Graphics2D)g;
        if (barriers != null)
        	drawBarriers(g2d);
//        drawGraph(g2d);
        drawRobot(g2d);
        drawTarget(g2d);
    }
    
    private static void fillOval(Graphics g, int centerX, int centerY, int diam1, int diam2)
    {
        g.fillOval(centerX - diam1 / 2, centerY - diam2 / 2, diam1, diam2);
    }
    
    private static void drawOval(Graphics g, int centerX, int centerY, int diam1, int diam2)
    {
        g.drawOval(centerX - diam1 / 2, centerY - diam2 / 2, diam1, diam2);
    }
    
    private void drawRobot(Graphics2D g)
    {
        int robotCenterX = round(robot.getX()); 
        int robotCenterY = round(robot.getY());
        AffineTransform t = AffineTransform.getRotateInstance(robot.getDirection(),
        		robotCenterX, robotCenterY); 
        g.setTransform(t);
        g.setColor(Color.MAGENTA);
        fillOval(g, robotCenterX, robotCenterY, 30, robotRadius);
        g.setColor(Color.BLACK);
        drawOval(g, robotCenterX, robotCenterY, 30, robotRadius);
        g.setColor(Color.WHITE);
        fillOval(g, robotCenterX  + robotRadius, robotCenterY, 5, 5);
        g.setColor(Color.BLACK);
        drawOval(g, robotCenterX  + robotRadius, robotCenterY, 5, 5);
    }
    
    private void drawBarriers(Graphics2D g) {
    	for (Barrier barrier : barriers) {
    		ArrayList<Point> points = barrier.getVertices();
    		g.setColor(barrier.getColor());
    		g.fillRect(
    				points.get(0).getX(),
    				points.get(0).getY(),
    				points.get(1).getX() - points.get(0).getX(),
    				points.get(3).getY() - points.get(0).getY());
    		g.setColor(Color.black);
    		g.drawRect(
    				points.get(0).getX(),
    				points.get(0).getY(),
    				points.get(1).getX() - points.get(0).getX(),
    				points.get(3).getY() - points.get(0).getY());
    	}
    }
    
    @SuppressWarnings("unused")
	private void drawGraph(Graphics2D g) {
    	g.setColor(Color.red);
    	for (Edge edge : graph.getEdges()) {
    		g.drawLine(
    				edge.getThisVertex().getX(),
    				edge.getThisVertex().getY(),
    				edge.getThatVertex().getX(),
    				edge.getThatVertex().getY());
    	}
    }
     
    private static int round(double value)
    {
        return (int)(value + 0.5);
    }
    
    private void drawTarget(Graphics2D g)
    {
        AffineTransform t = AffineTransform.getRotateInstance(0, 0, 0); 
        g.setTransform(t);
        g.setColor(Color.GREEN);
        fillOval(g, targetPoint.getX(), targetPoint.getY(), 5, 5);
        g.setColor(Color.BLACK);
        drawOval(g, targetPoint.getX(), targetPoint.getY(), 5, 5);
    }
}
