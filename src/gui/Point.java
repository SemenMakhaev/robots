package gui;

import java.util.ArrayList;

@SuppressWarnings("rawtypes")
public final class Point implements Comparable {
	
	private final int X;
	private final int Y;
	private ArrayList<Edge> incidentEdges;
	private boolean visited;
	
	public Point(int x, int y) {
		X = x;
		Y = y;
		incidentEdges = new ArrayList<Edge>();
		visited = false;
	}
	
	public int getX() {
		return X;
	}
	
	public int getY() {
		return Y;
	}
	
	public void visit() {
		visited = true;
	}
	
	public boolean isVisited() {
		return visited;
	}
	
	public ArrayList<Edge> getIncidentEdges() {
		return incidentEdges;
	}
	
	public void addIncidentEdge(Edge edge)
	{
		boolean alreadyAdded = false;
		
		for (Edge ourEdge : incidentEdges)
		{
			if (edge == ourEdge)
			{
				alreadyAdded = true;
				break;
			}
		}
		
		if (!alreadyAdded)
			incidentEdges.add(edge);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Point))
			return false;
		Point point = (Point) obj;
		return X == point.X && Y == point.Y;
	}

	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
